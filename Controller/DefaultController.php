<?php

namespace TMWK\PrestashopIntegrationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TMWKPrestashopIntegrationBundle:Default:index.html.twig');
    }
}
