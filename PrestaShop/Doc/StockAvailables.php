<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/10/2017
 * Time: 10:21
 */

namespace AppBundle\PrestaShop\Doc;


class StockAvailables
{
    /**
     * @var StockAvailableDetails[]
     */
    public $stock_availables;
}