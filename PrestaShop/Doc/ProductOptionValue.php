<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/10/2017
 * Time: 16:40
 */

namespace AppBundle\PrestaShop\Doc;


class ProductOptionValue
{
    /**
     * @var ProductOptionValueDetails
     */
    public $product_option_value;
}