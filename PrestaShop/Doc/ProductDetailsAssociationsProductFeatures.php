<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/10/2017
 * Time: 9:38
 */

namespace AppBundle\PrestaShop\Doc;


class ProductDetailsAssociationsProductFeatures
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var integer
     */
    public $id_feature_value;
}