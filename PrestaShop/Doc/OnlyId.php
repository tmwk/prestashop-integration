<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/10/2017
 * Time: 9:41
 */

namespace AppBundle\PrestaShop\Doc;


class OnlyId
{
    /**
     * @var integer
     */
    public $id;
}