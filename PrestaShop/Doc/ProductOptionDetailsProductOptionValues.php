<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/10/2017
 * Time: 16:24
 */

namespace AppBundle\PrestaShop\Doc;


class ProductOptionDetailsProductOptionValues
{
    /**
     * @var OnlyId[]
     */
    public $product_option_values;
}