<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/10/2017
 * Time: 16:22
 */

namespace AppBundle\PrestaShop\Doc;


class ProductOptions
{
    /**
     * @var ProductOptionDetails[]
     */
    public $product_options;
}