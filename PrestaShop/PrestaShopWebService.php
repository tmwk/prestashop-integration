<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 06/10/2017
 * Time: 8:22
 */

namespace TMWK\PrestashopIntegrationBundle\PrestaShop;

use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Addresses;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Carriers;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\CartRules;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Carts;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Categories;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Combinations;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Configurations;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Contacts;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\ContentManagementSystem;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Countries;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Currencies;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\CustomerMessages;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Customers;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\CustomerThreads;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Deliveries;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Employees;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Groups;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Guests;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Images;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\ImageTypes;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Languages;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Manufacturers;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\OrderCarriers;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\OrderDetails;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\OrderDiscounts;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\OrderHistories;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\OrderInvoices;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\OrderPayments;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Orders;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\OrderStates;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\PriceRanges;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\ProductFeatures;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\ProductFeatureValues;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\ProductOptions;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\ProductOptionValues;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Products;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\ProductSuppliers;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Search;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\ShopGroups;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Shops;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\SpecificPriceRules;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\SpecificPrices;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\States;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\StockAvailables;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\StockMovementReasons;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\StockMovements;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Stocks;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Stores;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Suppliers;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\SupplyOrderDetails;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\SupplyOrderHistories;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\SupplyOrderReceiptHistories;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\SupplyOrders;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\SupplyOrderStates;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Tags;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Taxes;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\TaxRuleGroups;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\TaxRules;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\TranslatedConfigurations;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\WarehouseProductLocations;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Warehouses;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\WeightRanges;
use TMWK\PrestashopIntegrationBundle\PrestaShop\Lib\Zones;

class PrestaShopWebService
{
    private static $_config;

    /**
     * PrestaShopWebService constructor.
     * @param $ps_url
     * @param $ps_key
     * @param $ps_debug
     */
    public function __construct($ps_url, $ps_key, $ps_debug)
    {
        $config = new Config();
        $config::setUrl($ps_url);
        $config::setKey($ps_key);
        $config::setDebug($ps_debug);
        self::$_config = $config;
    }

    /**
     * @return Addresses
     */
    public function Addresses()
    {
        return new Addresses();
    }

    /**
     * @return Carriers
     */
    public function Carriers()
    {
        return new Carriers();
    }

    /**
     * @return CartRules
     */
    public function CartRules()
    {
        return new CartRules();
    }

    /**
     * @return Carts
     */
    public function Carts()
    {
        return new Carts();
    }

    /**
     * @return Categories
     */
    public function Categories()
    {
        return new Categories();
    }

    /**
     * @return Combinations
     */
    public function Combinations()
    {
        return new Combinations();
    }

    /**
     * @return Configurations
     */
    public function Configurations()
    {
        return new Configurations();
    }

    /**
     * @return Contacts
     */
    public function Contacts()
    {
        return new Contacts();
    }

    /**
     * @return ContentManagementSystem
     */
    public function ContentManagementSystem()
    {
        return new ContentManagementSystem();
    }

    /**
     * @return Countries
     */
    public function Countries()
    {
        return new Countries();
    }

    /**
     * @return Currencies
     */
    public function Currencies()
    {
        return new Currencies();
    }

    /**
     * @return CustomerMessages
     */
    public function CustomerMessages()
    {
        return new CustomerMessages();
    }

    /**
     * @return Customers
     */
    public function Customers()
    {
        return new Customers();
    }

    /**
     * @return CustomerThreads
     */
    public function CustomerThreads()
    {
        return new CustomerThreads();
    }

    /**
     * @return Deliveries
     */
    public function Deliveries()
    {
        return new Deliveries();
    }

    /**
     * @return Employees
     */
    public function Employees()
    {
        return new Employees();
    }

    /**
     * @return Groups
     */
    public function Groups()
    {
        return new Groups();
    }

    /**
     * @return Guests
     */
    public function Guests()
    {
        return new Guests();
    }

    /**
     * @return Images
     */
    public function Images()
    {
        return new Images();
    }

    /**
     * @return ImageTypes
     */
    public function ImageTypes()
    {
        return new ImageTypes();
    }

    /**
     * @return Languages
     */
    public function Languages()
    {
        return new Languages();
    }

    /**
     * @return Manufacturers
     */
    public function Manufacturers()
    {
        return new Manufacturers();
    }

    /**
     * @return OrderCarriers
     */
    public function OrderCarriers()
    {
        return new OrderCarriers();
    }

    /**
     * @return OrderDetails
     */
    public function OrderDetails()
    {
        return new OrderDetails();
    }

    /**
     * @return OrderDiscounts
     */
    public function OrderDiscounts()
    {
        return new OrderDiscounts();
    }

    /**
     * @return OrderHistories
     */
    public function OrderHistories()
    {
        return new OrderHistories();
    }

    /**
     * @return OrderInvoices
     */
    public function OrderInvoices()
    {
        return new OrderInvoices();
    }

    /**
     * @return OrderPayments
     */
    public function OrderPayments()
    {
        return new OrderPayments();
    }

    /**
     * @return Orders
     */
    public function Orders()
    {
        return new Orders();
    }

    /**
     * @return OrderStates
     */
    public function OrderStates()
    {
        return new OrderStates();
    }

    /**
     * @return PriceRanges
     */
    public function PriceRanges()
    {
        return new PriceRanges();
    }

    /**
     * @return ProductFeatures
     */
    public function ProductFeatures()
    {
        return new ProductFeatures();
    }

    /**
     * @return ProductFeatureValues
     */
    public function ProductFeatureValues()
    {
        return new ProductFeatureValues();
    }

    /**
     * @return ProductOptions
     */
    public function ProductOptions()
    {
        return new ProductOptions();
    }

    /**
     * @return ProductOptionValues
     */
    public function ProductOptionValues()
    {
        return new ProductOptionValues();
    }

    /**
     * @return Products
     */
    public function Products()
    {
        return new Products();
    }

    /**
     * @return ProductSuppliers
     */
    public function ProductSuppliers()
    {
        return new ProductSuppliers();
    }

    /**
     * @return Search
     */
    public function Search()
    {
        return new Search();
    }

    /**
     * @return ShopGroups
     */
    public function ShopGroups()
    {
        return new ShopGroups();
    }

    /**
     * @return Shops
     */
    public function Shops()
    {
        return new Shops();
    }

    /**
     * @return SpecificPriceRules
     */
    public function SpecificPriceRules()
    {
        return new SpecificPriceRules();
    }

    /**
     * @return SpecificPrices
     */
    public function SpecificPrices()
    {
        return new SpecificPrices();
    }

    /**
     * @return States
     */
    public function States()
    {
        return new States();
    }

    /**
     * @return StockAvailables
     */
    public function StockAvailables()
    {
        return new StockAvailables();
    }

    /**
     * @return StockMovementReasons
     */
    public function StockMovementReasons()
    {
        return new StockMovementReasons();
    }

    /**
     * @return StockMovements
     */
    public function StockMovements()
    {
        return new StockMovements();
    }

    /**
     * @return Stocks
     */
    public function Stocks()
    {
        return new Stocks();
    }

    /**
     * @return Stores
     */
    public function Stores()
    {
        return new Stores();
    }

    /**
     * @return Suppliers
     */
    public function Suppliers()
    {
        return new Suppliers();
    }

    /**
     * @return SupplyOrderDetails
     */
    public function SupplyOrderDetails()
    {
        return new SupplyOrderDetails();
    }

    /**
     * @return SupplyOrderHistories
     */
    public function SupplyOrderHistories()
    {
        return new SupplyOrderHistories();
    }

    /**
     * @return SupplyOrderReceiptHistories
     */
    public function SupplyOrderReceiptHistories()
    {
        return new SupplyOrderReceiptHistories();
    }

    /**
     * @return SupplyOrders
     */
    public function SupplyOrders()
    {
        return new SupplyOrders();
    }

    /**
     * @return SupplyOrderStates
     */
    public function SupplyOrderStates()
    {
        return new SupplyOrderStates();
    }

    /**
     * @return Tags
     */
    public function Tags()
    {
        return new Tags();
    }

    /**
     * @return Taxes
     */
    public function Taxes()
    {
        return new Taxes();
    }

    /**
     * @return TaxRuleGroups
     */
    public function TaxRuleGroups()
    {
        return new TaxRuleGroups();
    }

    /**
     * @return TaxRules
     */
    public function TaxRules()
    {
        return new TaxRules();
    }

    /**
     * @return TranslatedConfigurations
     */
    public function TranslatedConfigurations()
    {
        return new TranslatedConfigurations();
    }

    /**
     * @return WarehouseProductLocations
     */
    public function WarehouseProductLocations()
    {
        return new WarehouseProductLocations();
    }

    /**
     * @return Warehouses
     */
    public function Warehouses()
    {
        return new Warehouses();
    }

    /**
     * @return WeightRanges
     */
    public function WeightRanges()
    {
        return new WeightRanges();
    }

    /**
     * @return Zones
     */
    public function Zones()
    {
        return new Zones();
    }

}