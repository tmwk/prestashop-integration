<?php
/**
 * Created by PhpStorm.
 * User: Mario Figueroa
 * Emaul: mfigueroa@enexum.cl
 * Date: 11-12-2017
 * Time: 8:34
 */

namespace AppBundle\PrestaShop;


use SimpleXMLElement;

class Utils
{
    public function toObject($data)
    {
        if(is_array($data)){
            return $data;
        }else{
            return simplexml_load_string($data);
        }
    }

    public function toObjectCorrection($data)
    {
        $result = $this->fixUTF8('<?xml version="1.0"?><response>' . $data . "</response>");
        return simplexml_load_string($result);
    }

    protected function fixUTF8($str)
    {
        return preg_replace_callback('#[\\xA1-\\xFF](?![\\x80-\\xBF]{2,})#', function ($m) {
            return utf8_encode($m[0]);
        }, $str);
    }

    public function toXml(SimpleXMLElement $object, array $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $new_object = $object->addChild($key);
                $this->toXml($new_object, $value);
            } else {
                // if the key is an integer, it needs text with it to actually work.
                if ($key == (int) $key) {
                    $key = "$key";
                }

                $object->addChild($key, $value);
            }
        }

        return true;
    }
}